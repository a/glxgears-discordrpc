# glxgears with discord rpc

I'm so sorry.

![](https://asa.cease-and-desisted.me/i/l0ev6bd7.png)

---

**dependencies:**

As far as I can see, you basically need:
- discord-rpc-api ([AUR](https://aur.archlinux.org/packages/discord-rpc-api-git/), [a copy of it available here](https://asa.cease-and-desisted.me/i/skofp3lo.so))
- GLEW ([arch](https://www.archlinux.org/packages/extra/x86_64/glew/))

perhaps some other things? Just install `mesa-demos` and `discord-rpc-api-git` and you should have everything you need.

---

Compiled binary for Linux x86_64 can be found [here](https://asa.cease-and-desisted.me/i/hghgmebb).

```bash
wget https://asa.cease-and-desisted.me/i/hghgmebb -O glxgearsrpc
wget https://asa.cease-and-desisted.me/i/skofp3lo.so -O libdiscord-rpc.so
chmod +x glxgearsrpc
./glxgearsrpc
```

